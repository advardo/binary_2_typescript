export async function parse(films: any[], type: string) {
    if (type === 'like') {
        parseFavourites(films);
    } else {
        const imgPath = 'https://image.tmdb.org/t/p/original/';
        const filmContainer = document.getElementById('film-container');
        filmContainer.innerHTML = '';
        let color;
        let random = Math.floor(Math.random() * films.length);
        document.getElementById('random-movie').style.backgroundImage = `url(${
            imgPath + films[random].backdrop_path
        })`;
        document.getElementById('random-movie-name').innerHTML =
            films[random].title;
        document.getElementById('random-movie-description').innerHTML =
            films[random].overview;
        for (let i = 0; i < films.length; i++) {
            if (localStorage.favourites.includes(films[i].id)) {
                color = 'red';
            } else {
                color = '#ff000078';
            }

            var component = `<div class="card shadow-sm">
                                <img
                                    src="${imgPath + films[i].poster}"
                                />
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    stroke="red"
                                    fill="${color}"
                                    width="50"
                                    height="50"
                                    class="bi bi-heart-fill position-absolute p-2"
                                    viewBox="0 -2 18 22"
                                >
                                    <path
                                        id="${films[i].id}"
                                        fill-rule="evenodd"
                                        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                                    />
                                </svg>
                                <div class="card-body">
                                    <p class="card-text truncate">
                                        ${films[i].overview}
                                    </p>
                                    <div
                                        class="
                                            d-flex
                                            justify-content-between
                                            align-items-center
                                        "
                                    >
                                        <small class="text-muted">${
                                            films[i].release_date
                                        }</small>
                                    </div>
                                </div>
                            </div>`;

            filmContainer.innerHTML += `<div class="col-lg-3 col-md-4 col-12 p-2">
                            ${component}
                        </div>`;
        }
    }
}

function parseFavourites(film: any): void {
    const imgPath = 'https://image.tmdb.org/t/p/original/';
    const fav = document.getElementById('favorite-movies');
    let color = 'red';

    fav.innerHTML += `<div class="col-12 p-2">
                    <div class="card shadow-sm">
                        <img
                            src="${imgPath + film.poster_path}"
                        />
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            stroke="red"
                            fill="red"
                            width="50"
                            height="50"
                            class="bi bi-heart-fill position-absolute p-2"
                            viewBox="0 -2 18 22"
                        >
                            <path
                                fill-rule="evenodd"
                                id="${film.id}"
                                d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                            />
                        </svg>
                        <div class="card-body">
                            <p class="card-text truncate">
                                ${film.overview}
                            </p>
                            <div
                                class="
                                    d-flex
                                    justify-content-between
                                    align-items-center
                                "
                            >
                                <small class="text-muted">${
                                    film.release_date
                                }</small>
                            </div>
                        </div>
                    </div>
                </div>`;
}
