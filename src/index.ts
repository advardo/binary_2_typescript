import { mapToStyles } from '@popperjs/core/lib/modifiers/computeStyles';
import { parse } from './helpers/parserHelper';

export async function render(): Promise<void> {
    // TODO render your app here

    const api_key = '25611a3ae1611e785f628be496afbba0';
    const link = 'https://api.themoviedb.org/3/';
    interface Movie {
        id: number;
        title: string;
        poster: string | null;
        overview: string;
        release_date: string;
        backdrop_path: string;
    }

    type MapperFunc<T, K> = (element: T) => K;

    const movieMapper = <T, K>(mapper: MapperFunc<T, K>, movies: T[]) => {
        const list = [];
        for (const el of movies) {
            const result = mapper(el);
            list.push(result);
        }
        return list;
    };

    const renderMovies = async function (url: string, type: string) {
        await fetch(url)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                if (type === 'like') {
                    parse(data, type);
                } else {
                    const newArray = movieMapper<any, Movie>((item) => {
                        return {
                            id: item.id,
                            title: item.title,
                            poster: item.poster_path,
                            overview: item.overview,
                            release_date: item.release_date,
                            backdrop_path: item.backdrop_path,
                        };
                    }, data.results);
                    parse(newArray, type);
                }
            });
    };

    if (!localStorage.favouritess) {
        localStorage.setItem('favourites', JSON.stringify([]));
    }
    let favourites = Array.from(JSON.parse(localStorage.getItem('favourites')));

    const likeMovie = function (movieId: string): void {
        const heart = document.getElementById(movieId);
        if (!favourites.includes(movieId)) {
            favourites.push(movieId);
            heart.style = 'fill: red';
        } else {
            heart.style = 'fill: #ff000078';
            favourites = favourites.filter((item) => item != movieId);
        }
        localStorage.setItem('favourites', JSON.stringify(favourites));
    };

    renderMovies(
        `${link}movie/popular?api_key=${api_key}&language=en-US&page=1`,
        'container'
    );
    document.body.addEventListener('click', function (e: MouseEvent) {
        var target = e.target;
        if (target?.nodeName === 'path') {
            likeMovie(target.id);
        }
        if (target.id === 'popular') {
            renderMovies(
                `${link}movie/popular?api_key=${api_key}&language=en-US&page=1`,
                'container'
            );
        }
        if (target.id === 'upcoming') {
            renderMovies(
                `${link}movie/upcoming?api_key=${api_key}&language=en-US&page=1`,
                'container'
            );
        }
        if (target.id === 'top_rated') {
            renderMovies(
                `${link}movie/top_rated?api_key=${api_key}&language=en-US&page=1`,
                'container'
            );
        }

        if (
            document.getElementById('offcanvasRight').style.visibility ===
            'visible'
        ) {
            document.getElementById('favorite-movies').innerHTML = '';
            favourites.forEach((item) =>
                renderMovies(
                    `${link}movie/${item}?api_key=${api_key}&language=en-US`,
                    'like'
                )
            );
        }

        if (target.id === 'submit') {
            const query = document.getElementById('search')?.value;
            renderMovies(
                `${link}search/movie?api_key=${api_key}&language=en-US&query=${query}&page=1&include_adult=false)`,
                'container'
            );
        }
        e.stopPropagation();
    });
}
